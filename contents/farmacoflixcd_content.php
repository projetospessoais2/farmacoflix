<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark-transparent" id="esconder-perfil1">
  <!-- Container wrapper -->
  <div class="container-fluid">
    <!-- Navbar brand -->
    <a class="navbar-brand" href="#">
      <img src="assets/farmacoflixcd/logoFlix.svg" height="30" alt="" loading="lazy" />
    </a>


    <div class="navbar-nav mb-lg-0 show-element-topnav">
      <!-- Navbar dropdown -->
      <div class="nav-item dropdown">
        <a class="nav-link dropdown-toggle dropbtn p-3" onclick="notificacoesResponsiva('showNotificationsCellphone')">
          <i class="fas fa-bell"></i>
          <!-- <img src="assets/farmacoflixcd/Ativo-1-1.svg" alt="" class="card-img"> -->
        </a>

        <div class="card dropdown-content grey-background" id="showNotificationsCellphone" style="right: 0;">
          <div class="card-body">

            <div class="row">
              <div class="col-4 col-md-4">
                <img src="assets/farmacoflixcd/comunidadeBanner.webp" alt="" class="card-img">
              </div>
              <div class="col-8 col-md-8 f-size-0-8 text-color-white">
                As inscrições para a Comunidade Farmaco na Prática com <strong>R$ 400</strong> de desconto abrem na quinta feira às 21h.
                <br><br>
                <a class="entrar-grupo-wpp text-weight-bolder" target="__blank" href="https://devzapp.com.br/api-engennier/campanha/api/redirect/629e3c0191d7a10001184817">Entrar no grupo do Whatsapp</a>

              </div>
            </div>
            <hr class="white-hr">

            <div class="row">
              <div class="col-4 col-md-4">
                <img src="assets/farmacoflixcd/lembreteYoutube.webp" alt="" class="card-img">
              </div>
              <div class="col-8 col-md-8 f-size-0-8 text-color-white">
                Defina o lembrete das aulas e ative as notificações para não perder nenhum detalhe desta série.
                <br><br>
                <a class="ativar-lembrete text-weight-bolder" target="__blank" href="https://youtube.com/c/farmacologianapratica">Ativar o lembrete</a>

              </div>
            </div>

          </div>
        </div>

      </div>

      <div class="nav-item dropdown">
        <a class="nav-link" href="#esconder-perfil1" id="navbarDropdown" role="button" data-mdb-toggle="dropdown"
          aria-expanded="false" onclick="abrirPerfis()">
          <img src="assets/farmacoflixcd/perfil2.webp" class="img-fluid change-user-image-cell"
            height='25' width='25'>
        </a>
      </div>

    </div>


    <!-- Toggle button -->
    <button class="navbar-toggler" type="button" data-mdb-toggle="collapse" data-mdb-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" onclick="menuResponsivo()">
      <i class="fas fa-bars" id="changeIconMenuResponsive"></i>
    </button>

    <!-- Collapsible wrapper -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      <!-- Left links -->
      <ul class="navbar-nav me-auto mb-2 mb-lg-0 centralizarObjetoCelular">
        <li class="nav-item">
          <a class="nav-link" href="#">Início</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="https://instagram.com/farmaconapratica?igshid=YmMyMTA2M2Y=" target="__blank">Instagram</a>
        </li>
      </ul>
      <!-- Left links -->

      
      <div class="navbar-nav mb-2 mb-lg-0 hide-element">
        <!-- Navbar dropdown -->
        <div class="nav-item dropdown direction-rtl">
          <a class="nav-link dropdown-toggle hidden-arrow dropbtn" onclick="notificacoesResponsiva('showNotificationsDesktop')">
            <i class="fas fa-bell"></i>
          </a>


          <div class="card dropdown-content grey-background" id="showNotificationsDesktop">
            <div class="card-body direction-ltr">

              <div class="row">
                <div class="col-md-4">
                  <img src="assets/farmacoflixcd/comunidadeBanner.webp" alt="" class="card-img">
                </div>
                <div class="col-md-8 f-size-0-8 text-color-white">
                  As inscrições para a Comunidade Farmaco na Prática com <strong>R$ 400</strong> de desconto abrem na quinta feira às 21h.
                  <br><br>
                  <a class="entrar-grupo-wpp text-weight-bolder" target="__blank" href="https://devzapp.com.br/api-engennier/campanha/api/redirect/629e3c0191d7a10001184817">Entrar no grupo do Whatsapp</a>

                </div>
              </div>

              <hr class="white-hr">

              <div class="row">
                <div class="col-md-4">
                  <img src="assets/farmacoflixcd/lembreteYoutube.webp" alt="" class="card-img">
                </div>
                <div class="col-md-8 f-size-0-8 text-color-white">
                  Defina o lembrete das aulas e ative as notificações para não perder nenhum detalhe desta série.
                  <br><br>
                  <a class="ativar-lembrete text-weight-bolder" target="__blank" href="https://www.youtube.com/c/farmacologianapratica/videos">Ativar o lembrete</a>

                </div>
              </div>

            </div>
          </div>

        </div>

        <div class="nav-item dropdown">
          <a class="nav-link" href="#" id="navbarDropdown" role="button" data-mdb-toggle="dropdown"
            aria-expanded="false" onclick="abrirPerfis()">
            <img src="assets/farmacoflixcd/perfil2.webp" class="img-fluid change-user-image-desk"
              height='25' width='25'>
          </a>
        </div>

      </div>

    </div>
    <!-- Collapsible wrapper -->
  </div>
  <!-- Container wrapper -->
</nav>
<!-- Navbar -->


<video autoplay muted playsinline loop id="myVideo">
    <source src="assets/farmacoflixcd/thumbnail/VIDEO_FUNDO.mp4" type="video/mp4">
</video>

<div class="show-element mt-neg-2"></div>

<div class="content" id="esconder-perfil2">
  <div class="show-element mt-neg-35"></div>
  <div class="row">
    <div class="logo-container row align-items-center">
        <img src="assets/farmacoflixcd/fLogo.svg" width="22px" height="50px" class="card-img card-img-logo flogo">
        <p class="logo-text px-0 mb-0">SÉRIE</p>
    </div>
    <div class="col-md-5">
      <img src="assets/farmacoflixcd/combatendo.webp" alt="" class="card-img mt-3">

      <div class="row">
        <div class="col-2 col-md-1-5 mt-3 float-left">
          <img src="assets/farmacoflixcd/top1.svg" width="30px" height="30px" class="card-img">
        </div>
        <div class="col-10 col-md-10 f-size-1-4 text-weight-bold mt-3">
          Internet Inteira: top 1 de hoje
        </div>
      </div>

      <div class="show-element mt-neg-10"></div>
      <p class="mt-5 font-weight-medium f-size-1">
        Uma série de aulas que irão te mostrar o que é a depressão, suas causas, consequências e como identificar e utilizar as melhores terapias medicamentosas com antidepressivos.
      </p>
    </div>
  </div>

  <div class="row">
    <div class="col-5 col-md-5">
      <a target="blank" href="https://www.youtube.com/c/farmacologianapratica">
        <button class="btn btn-netflix mt-2 col-12 col-md-3">
          <i class="fas fa-play"></i>
          Assistir
        </button>
      </a>
    </div>

    <div class="col-2 col-md-6 col-right remove-right mt-3">
    <div class="show-element mt-neg-15"></div>
      <button class="btn btn-volume position-relative" onclick="toggleMute()">
        <i aria-hidden="true" class="fas fa-volume-mute position-absolute" id="myIcon"></i>
      </button>
    </div>

    <div class="col-md-1 mt-3 hide-element">  
      <div class="row">

        <div class="col-md-12">

          <div class="card card-background-age bg-dark-transparent">
            <div class="card-body">
    
              <div class="card red-background card-age-red">
                <div class="card-body f-size-1 text-weight-bolder">
                  14
                </div>
              </div>
    
            </div>
          </div>

        </div>
      </div>
      
    </div>
  </div>


  </div>
</div>
</div>


<div class="container-fluid grey-background" id="esconder-perfil3">

  <div class="row mt-neg-2 hide-element">
    <div class="col-md-3 container-cards">

      <div class="card-banner-hover">
        <div>
          <img src="assets/farmacoflixcd/thumbnail/thumbEP1.webp" alt="" class="card-img">
        </div>

        <p class="text-color-white f-size-1 text-weight-bolder">Conhecendo a depressão: qual a sua origem e consequências?</p>

        <div class="content-conteudo">
          <div class="content-video">
            <video autoplay muted playsinline loop height="100%" width="100%" id="videos-banners">
                <source src="assets/farmacoflixcd/thumbnail/AULA_01.mp4" type="video/mp4">
            </video>
          </div>

          <div class="controls-video">
            
              <a href="https://www.youtube.com/watch?v=wQGq-rA1jno" target="__blank">
                <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
              </a>
              <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 1)"><i class="fas fa-thumbs-up"></i></button>
              <span class="relevance f-size-0-6">100% relevante</span>
              <span class="text-weight-bold f-size-0-6">Aula 1 de 4</span>
          </div>

          <a href="https://www.youtube.com/watch?v=wQGq-rA1jno" target="__blank">
            <button class="btn btn-definir-lembrete col-12 f-size-1-2 lh-1-2 text-center">
                <i class="fas fa-play"></i>
                <span class="p-2">Assista Agora</span> 
            </button>
          </a>

          <div class="p-2">
              <p class="title-ep f-size-0-8 text-weight-bolder mt-2">Episódio 01 - 12/09 - 20H</p>

              <p class="text-weight-bolder text-color-white tituloEpisodio mt-2">Conhecendo a depressão: qual a sua origem e consequências?</p>

              <p class="f-size-0-7" style="color: rgb(115, 115, 115);">
                Apesar de clássicas descrições bíblicas e mitológicas, veja que curioso: o termo “depressão” para designar um estado de desânimo ou perda de interesse surgiu somente no século XVII. E não acabou por aí: seus aspectos fisiopatológicos foram mais conhecidos somente nos últimos 70 anos. Neste episódio você vai descobrir tudo o que está por trás da famosa “falta de serotonina…” Seria somente isso? Qual exame poderia auxiliar nesse diagnóstico? Quais sinais o profissional de saúde precisa ficar bem atento? Se eu fosse você, não perderia nem um minuto desse episódio.

                  <br><br>
                  Farmacologia • Antidepressivos • Saúde
              </p>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-3 container-cards">

      <div class="card-banner-hover">
        <div>
          <img src="assets/farmacoflixcd/thumbnail/thumbEP2.webp" alt="" class="card-img">
        </div>

        <p class="text-color-white f-size-1 text-weight-bolder">Como tratar a depressão sem o uso de medicamentos?</p>

        <div class="content-conteudo">
          <div class="content-video">
            <video autoplay muted playsinline loop height="100%" width="100%" id="videos-banners">
                <source src="assets/farmacoflixcd/thumbnail/AULA_02.mp4" type="video/mp4">
            </video>
          </div>

          <div class="controls-video">
            
              <a href="https://www.youtube.com/watch?v=FHmdnLLj0ak" target="__blank">
                <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
              </a>
              <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 2)"><i class="fas fa-thumbs-up"></i></button>
              <span class="relevance f-size-0-6">100% relevante</span>
              <span class="text-weight-bold f-size-0-6">Aula 2 de 4</span>
          </div>

          <a href="https://www.youtube.com/watch?v=FHmdnLLj0ak" target="__blank">
            <button class="btn btn-definir-lembrete col-12 f-size-1-2 lh-1-2 text-center">
                <i class="fas fa-play"></i>
                <span class="p-2">Assista Agora</span> 
            </button>
          </a>

          <div class="p-2">
              <p class="title-ep f-size-0-8 text-weight-bolder mt-2">Episódio 02• 13/09 • 20H</p>

              <p class="text-weight-bolder text-color-white tituloEpisodio mt-2">Como tratar a depressão sem o uso de medicamentos?</p>

              <p class="f-size-0-7" style="color: rgb(115, 115, 115);">
              Depois de entender os mecanismos que ajudam no “disparo e consequências da depressão”, que tal conhecer diferentes estratégias não-medicamentosas que as evidências científicas tem mostrado para combatê-la? O que é mito ou verdade? Neste episódio, você vai entender por que os medicamentos são apenas um dos meros recursos terapêuticos e que sem as medidas não-farmacológicas a chance de insucesso no tratamento antidepressivo é enorme.

              <br><br>

              Farmacologia • Antidepressivos • Saúde
              </p>
          </div>
        </div>
      </div>

    </div>

    <div class="col-md-3 container-cards">

      <div class="card-banner-hover">
        <div>
          <img src="assets/farmacoflixcd/thumbnail/thumbEP3.webp" alt="" class="card-img">
        </div>

        <p class="text-color-white f-size-1 text-weight-bolder">Como definir o melhor antidepressivo para cada situação?</p>

        <div class="content-conteudo">
          <div class="content-video">
            <video autoplay muted playsinline loop height="100%" width="100%" id="videos-banners">
                <source src="assets/farmacoflixcd/thumbnail/AULA_04.mp4" type="video/mp4">
            </video>
          </div>

          <div class="controls-video">
            
            <a href="https://www.youtube.com/watch?v=lUxzCC5o4F0" target="__blank">
              <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
            </a>
            <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 3)"><i class="fas fa-thumbs-up"></i></button>
            <span class="relevance f-size-0-6">100% relevante</span>
            <span class="text-weight-bold f-size-0-6">Aula 3 de 4</span>
          </div>

          <a href="https://www.youtube.com/watch?v=lUxzCC5o4F0" target="__blank">
            <button class="btn btn-definir-lembrete col-12 f-size-1-2 lh-1-2 text-center">
                <i class="fas fa-play"></i>
                <span class="p-2">Assista Agora</span> 
            </button>
          </a>

          <div class="p-2">
              <p class="title-ep f-size-0-8 text-weight-bolder mt-2">Episódio 03 • 14/09 • 20H</p>

              <p class="text-weight-bolder text-color-white tituloEpisodio mt-2">Como definir o melhor antidepressivo para cada situação?</p>

              <p class="f-size-0-7" style="color: rgb(115, 115, 115);">
              Neste episódio, você vai compreender as diversas ferramentas farmacológicas antidepressivas disponíveis na atualidade. A partir desse domínio, vai ficar bem mais fácil responder perguntas como: “por que pra ela foi a sertralina e não a venlafaxina”? “Por que para aquele rapaz foi citalopram com bupropiona”? “E para aquele idoso, por que trazodona”? “E por que com aquele paciente oncológico contra-indicamos fluoxetina ou amitriptilina?”. Nesse episódio você vai conhecer as principais classes de depressivos e aprender como e quando usá-los.

              <br><br>

              Farmacologia • Antidepressivos • Saúde
              </p>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-3 container-cards">

      <div class="card-banner-hover">
        <div>
          <img src="assets/farmacoflixcd/thumbnail/thumbEP4.webp" alt="" class="card-img">
        </div>

        <p class="text-color-white f-size-1 text-weight-bolder">Combatendo a depressão na prática: discutindo casos clínicos</p>

        <div class="content-conteudo">
          <div class="content-video">
            <video autoplay muted playsinline loop height="100%" width="100%" id="videos-banners">
                <source src="assets/farmacoflixcd/thumbnail/AULA_03.mp4" type="video/mp4">
            </video>
          </div>

          <div class="controls-video">
            
            <a href="https://www.youtube.com/watch?v=Ka7lofC4bIs" target="__blank">
              <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
            </a>
            <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 4)"><i class="fas fa-thumbs-up"></i></button>
            <span class="relevance f-size-0-6">100% relevante</span>
            <span class="text-weight-bold f-size-0-6">Aula 4 de 4</span>
          </div>

          <a href="https://www.youtube.com/watch?v=Ka7lofC4bIs" target="__blank">
            <button class="btn btn-definir-lembrete col-12 f-size-1-2 lh-1-2 text-center">
                <i class="fas fa-play"></i>
                <span class="p-2">Assista Agora</span> 
            </button>
          </a>

          <div class="p-2">
              <p class="title-ep f-size-0-8 text-weight-bolder mt-2">Episódio 04 • 15/09 • 20H</p>

              <p class="text-weight-bolder text-color-white tituloEpisodio mt-2">Combatendo a depressão na prática: discutindo casos clínicos</p>

              <p class="f-size-0-7" style="color: rgb(115, 115, 115);">Chegou a hora de testar seus conhecimentos. Como integrante de uma equipe multiprofissional, além de saber sobre as principais classes de antidepressivos, existem habilidades fundamentais para a substituição de antidepressivos, ajustes posológicos, associação com outras classes e claro, a identificação de potenciais interações medicamentosas. Através de casos clínicos do cotidiano brasileiro, você vai poder aplicar diversas estratégias para garantir boa adesão ao tratamento do paciente depressivo livrando-o de possíveis comorbidades e melhorando sua qualidade de vida. Não perca!

              <br><br>

              Farmacologia • Antidepressivos • Saúde
              </p>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="row show-element">
    <div class="col-1"></div>
    <div class="col-10 video-card">
      <div class="content-conteudo">
              <div class="content-video">
                <video autoplay muted playsinline loop width="100%" id="videos-banners">
                    <source src="assets/farmacoflixcd/thumbnail/AULA_01.mp4" type="video/mp4">
                </video>
              </div>
              <div class="controls-video p-3">
      
                  <a href="https://www.youtube.com/watch?v=wQGq-rA1jno" target="__blank">
                    <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
                  </a>
                  <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 1)"><i class="fas fa-thumbs-up"></i></button>
                  <span class="relevance f-size-0-6 p-2">100% relevante</span>
                  <span class="text-weight-bold f-size-0-6">Aula 1 de 4</span>
              </div>
              <div class="p-3">
                  <p class="title-ep f-size-0-8 text-weight-light mt-neg-4">Episódio 01 - 12/09 - 20H</p>
                  <p class="f-size-0-8 text-weight-bolder tituloEpisodio text-color-white mt-neg-2">Conhecendo a depressão: qual a sua origem e consequências?</p>

                  <a href="https://www.youtube.com/watch?v=wQGq-rA1jno" target="__blank">
                    <button class="btn btn-definir-lembrete col-9 f-size-1 lh-1-2 text-center mt-neg-2">
                        <i class="fas fa-play"></i>
                        <span class="p-2">Assista Agora</span>
                    </button>
                  </a>

                  <p class="f-size-0-8 mt-2 text-color-white">
                    Apesar de clássicas descrições bíblicas e mitológicas, veja que curioso: o termo “depressão” para designar um estado de desânimo ou perda de interesse surgiu somente no século XVII. E não acabou por aí: seus aspectos fisiopatológicos foram mais conhecidos somente nos últimos 70 anos. Neste episódio você vai descobrir tudo o que está por trás da famosa “falta de serotonina…” Seria somente isso? Qual exame poderia auxiliar nesse diagnóstico? Quais sinais o profissional de saúde precisa ficar bem atento? Se eu fosse você, não perderia nem um minuto desse episódio.
                  
                      <br><br>
                      Farmacologia • Antidepressivos • Saúde
                  </p>
              </div>
        </div>
    </div>
    <div class="col-1"></div>
  </div>

  <div class="row show-element mt-5">
    <div class="col-1"></div>
    <div class="col-10 video-card">
      <div class="content-conteudo">
        <div class="content-video">
          <video autoplay muted playsinline loop width="100%" id="videos-banners">
              <source src="assets/farmacoflixcd/thumbnail/AULA_02.mp4" type="video/mp4">
          </video>
        </div>

        <div class="controls-video  p-3">
            <a href="https://www.youtube.com/watch?v=FHmdnLLj0ak" target="__blank">
              <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
            </a>
            <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 2)"><i class="fas fa-thumbs-up"></i></button>
            <span class="relevance f-size-0-6 p-2">100% relevante</span>
            <span class="text-weight-bold f-size-0-6">Aula 2 de 4</span>
        </div>

        <div class="p-3">

          <p class="title-ep f-size-0-8 text-weight-light mt-neg-4">Episódio 02• 13/09 • 20H</p>
          <p class="f-size-0-8 text-weight-bolder tituloEpisodio text-color-white mt-neg-2">Como tratar a depressão sem o uso de medicamentos?</p>

          <a href="https://www.youtube.com/watch?v=FHmdnLLj0ak" target="__blank">
            <button class="btn btn-definir-lembrete col-9 f-size-1 lh-1-2 text-center mt-neg-2">
                <i class="fas fa-play"></i>
                <span class="p-2">Assista Agora</span>
            </button>
          </a>

          <p class="f-size-0-8 mt-2 text-color-white">
            Depois de entender os mecanismos que ajudam no “disparo e consequências da depressão”, que tal conhecer diferentes estratégias não-medicamentosas que as evidências científicas tem mostrado para combatê-la? O que é mito ou verdade? Neste episódio, você vai entender por que os medicamentos são apenas um dos meros recursos terapêuticos e que sem as medidas não-farmacológicas a chance de insucesso no tratamento antidepressivo é enorme.

          <br><br>

          Farmacologia • Antidepressivos • Saúde
          </p>
        </div>
      </div>
    </div>
    <div class="col-1"></div>
  </div>

  <div class="row show-element mt-5">
    <div class="col-1"></div>
    <div class="col-10 video-card">
      <div class="content-conteudo">
        <div class="content-video">
          <video autoplay muted playsinline loop width="100%" id="videos-banners">
              <source src="assets/farmacoflixcd/thumbnail/AULA_04.mp4" type="video/mp4">
          </video>
        </div>

        <div class="controls-video  p-3">
          
          <a href="https://www.youtube.com/watch?v=lUxzCC5o4F0" target="__blank">
            <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
          </a>
          <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 3)"><i class="fas fa-thumbs-up"></i></button>
          <span class="relevance f-size-0-6 p-2">100% relevante</span>
          <span class="text-weight-bold f-size-0-6">Aula 3 de 4</span>
        </div>

        <div class="p-3">

          <p class="title-ep f-size-0-8 text-weight-light mt-neg-4">Episódio 03 • 14/09 • 20H</p>
          <p class="f-size-0-8 text-weight-bolder tituloEpisodio text-color-white mt-neg-2">Como definir o melhor antidepressivo para cada situação?</p>

          <a href="https://www.youtube.com/watch?v=lUxzCC5o4F0" target="__blank">
            <button class="btn btn-definir-lembrete col-9 f-size-1 lh-1-2 text-center mt-neg-2">
                <i class="fas fa-play"></i>
                <span class="p-2">Assista Agora</span>
            </button>
          </a>

          <p class="f-size-0-8 mt-2 text-color-white">
            Neste episódio, você vai compreender as diversas ferramentas farmacológicas antidepressivas disponíveis na atualidade. A partir desse domínio, vai ficar bem mais fácil responder perguntas como: “por que pra ela foi a sertralina e não a venlafaxina”? “Por que para aquele rapaz foi citalopram com bupropiona”? “E para aquele idoso, por que trazodona”? “E por que com aquele paciente oncológico contra-indicamos fluoxetina ou amitriptilina?”. Nesse episódio você vai conhecer as principais classes de depressivos e aprender como e quando usá-los.

          <br><br>

          Farmacologia • Antidepressivos • Saúde
          </p>
        </div>
      </div>
    </div>
    <div class="col-1"></div>
  </div>

  <div class="row show-element mt-5">
    <div class="col-1"></div>
    <div class="col-10 video-card">
      <div class="content-conteudo">
        <div class="content-video">
          <video autoplay muted playsinline loop width="100%" id="videos-banners">
              <source src="assets/farmacoflixcd/thumbnail/AULA_03.mp4" type="video/mp4">
          </video>
        </div>

        <div class="controls-video  p-3">
          
          <a href="https://www.youtube.com/watch?v=Ka7lofC4bIs" target="__blank">
            <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
          </a>
          <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 4)"><i class="fas fa-thumbs-up"></i></button>
          <span class="relevance f-size-0-6 p-2">100% relevante</span>
          <span class="text-weight-bold f-size-0-6">Aula 4 de 4</span>
        </div>

        <div class="p-3">

          <p class="title-ep f-size-0-8 text-weight-light mt-neg-4">Episódio 04 • 15/09 • 20H</p>
          <p class="f-size-0-8 text-weight-bolder tituloEpisodio text-color-white mt-neg-2">Combatendo a depressão na prática: discutindo casos clínicos</p>

          <a href="https://www.youtube.com/watch?v=Ka7lofC4bIs" target="__blank">
            <button class="btn btn-definir-lembrete col-9 f-size-1 lh-1-2 text-center mt-neg-2">
                <i class="fas fa-play"></i>
                <span class="p-2">Assista Agora</span>
            </button>
          </a>

          <p class="f-size-0-8 mt-2 text-color-white">
          Chegou a hora de testar seus conhecimentos. Como integrante de uma equipe multiprofissional, além de saber sobre as principais classes de antidepressivos, existem habilidades fundamentais para a substituição de antidepressivos, ajustes posológicos, associação com outras classes e claro, a identificação de potenciais interações medicamentosas. Através de casos clínicos do cotidiano brasileiro, você vai poder aplicar diversas estratégias para garantir boa adesão ao tratamento do paciente depressivo livrando-o de possíveis comorbidades e melhorando sua qualidade de vida. Não perca!

          <br><br>

          Farmacologia • Antidepressivos • Saúde
          </p>
        </div>
      </div>
    </div>
    <div class="col-1"></div>
  </div>

  <div class="show-element mt-neg-15"></div>
    <div class="row">
    <div class="col-md-6 mt-5" style="position: relative">
      <img src="assets/farmacoflixcd/bannerCelular.webp" alt="" class="card-img">
      <div class="hoverCountdown text-color-white">
        <span class="text-weight-bold f-size-1 countdown">&nbsp;&nbsp;&nbsp;As inscrições encerram em</span> <br> 
        <span class="text-weight-bolder f-size-2-6">&nbsp; <span id="days"></span></span>  
        <span class="text-weight-bold f-size-1 countdown"> Dias</span> <br>
        <span class="text-weight-bolder f-size-2">&nbsp;&nbsp;<span id="hours"></span> &nbsp;&nbsp;&nbsp; <span id="minutes"></span> &nbsp;&nbsp;&nbsp;<span id="seconds"></span></span> <br>
        <span class="text-weight-bolder f-size-1 countdown">&nbsp;&nbsp;&nbsp;&nbsp; HRS  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MINS  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SEG</span>
      </div>
    </div>
    <div class="col-md-6 mt-5 centralizarObjetoCelular">

    <div class="show-element mt-neg-15"></div>
      <div class="row">
        <div class="col-md-5 mt-5">
          <img src="assets/farmacoflixcd/comunidadeFarmaco.webp" alt="" class="card-img logo-farmacologiaPratica-2">
        </div>
      </div>
      <p class="text-weight-extrabold mt-3 f-size-1-3 text-color-white">
        <!-- SAVE THE DATE: 15/09 às 21h - Ao vivo -->
        INSCRIÇÕES ABERTAS
      </p>
      <p class="text-weight-bold mt-3 f-size-1-2 text-color-white">
        <!-- Inscrições para a Comunidade Farmaco na Prática -->
      </p>

      <!-- <p class="text-weight-bold mt-3 f-size-1-4 text-color-white">
        Inscrições para Comunidade Farmaco na Prática
      </p> -->


      <div class="row">
        <div class="col-md-3">
          <div class="card card-desconto text-center">
            <div class="card-body text-color-white f-size-1-2 lh-1-2">
              R$ 400 de desconto!
            </div>
          </div>
        </div>
        <div class="col-md-9">
          <div class="mt-3 show-element"></div>
          <a href="https://farmaconapratica.com.br/inscricao-comunidade-v7/" target="__blank">
            <button class="btn btn-wpp col-10 f-size-1-2 lh-1-2 text-center">
              <!-- <i class="fa-brands fa-whatsapp"></i> -->
              CLIQUE E GARANTA SUA VAGA
            </button>
          </a>
        </div>
      </div>

    </div>
  </div>
  
  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4 text-center mt-4 align-items-center">
      <img src="assets/farmacoflixcd/logoFarmacologiaPratica.webp" alt="" class="card-img logo-farmacologiaPratica">
    </div>
    <div class="col-md-4"></div>
  </div>

  <div class="row text-center text-color-white">
    <div class="col-md-4"></div>
    <div class="col-md-4">
      Copywriting
    </div>
    <div class="col-md-4"></div>
  </div>

  <br><br>

</div>

<div class="black-background background-perfis">

  <div class="row">
    <div class="col-10 col-md-11"></div>
    <div class="col-2 col-md-1 text-center p-2">
      <a class="close-perfis" href="#" onclick="fecharPerfis()">
        <i class="fas fa-times"></i>
      </a>
    </div>
  </div>

  <div class="row text-color-white f-size-1-9 text-weight-bolder">
    <div class="col-md-12 text-center mt-10">
      Como você se define?
    </div>
  </div>

  <div class="row">
    <div class="col-md-2"></div>

    <div class="col-1 show-element"></div>
    
    <div class="col-5 col-md-2 mt-6 text-center">
      <img src="assets/farmacoflixcd/perfil1.webp" onclick="changeUserProfilePicture(1)" class="card-img hover-perfil">
      <p class="text-color-white mt-2 lh-1-2">
        NÃO SEI NADA, ME AJUDA
      </p>
    </div>


    <div class="col-5 col-md-2 mt-6 text-center">
      <img src="assets/farmacoflixcd/perfil2.webp" onclick="changeUserProfilePicture(2)" class="card-img hover-perfil">
      <p class="text-color-white mt-2 lh-1-2">
        TÔ APRENDENDO E TO ANIMADO
      </p>
    </div>


    <div class="col-1 show-element"></div>

    <div class="col-1 show-element"></div>

    <div class="col-5 col-md-2 mt-6 text-center">
      <img src="assets/farmacoflixcd/perfil3.webp" onclick="changeUserProfilePicture(3)" class="card-img hover-perfil">
      <p class="text-color-white mt-2 lh-1-2">
        PRONTO PARA DOMINAR A FARMACO
      </p>
    </div>

    <div class="col-5 col-md-2 mt-6 text-center">
      <img src="assets/farmacoflixcd/perfil4.webp" onclick="changeUserProfilePicture(4)" class="card-img hover-perfil">
      <p class="text-color-white mt-2 lh-1-2">
        JÁ SOU DA COMUNIDADE MAS TO AQUI
      </p>
    </div>

    <div class="col-1 show-element"></div>

    <div class="col-md-2"></div>
  </div>

  <br><br><br><br>
  <br><br><br><br>

</div>