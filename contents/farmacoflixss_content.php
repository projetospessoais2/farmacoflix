<!-- VARIAVEIS -->
<?php 
  // EPISODIO 01
  $tituloEpisodio01 = 'Prelúdio: Conhecendo o Sistema Nervoso Autônomo';
  $textoEpisodio01  = 'No episódio de estreia, deixe de lado os métodos tradicionais e fique com os ouvidos e olhos atentos para assistir a uma abordagem inovadora que compara o sistema nervoso autônomo (SNA) a uma orquestra que funciona silenciosamente (ou melhor, sem o seu controle voluntário). Este episódio inicial já começa com informações envolventes e insights valiosos para você dominar de uma vez por todas a farmacologia do SNA. Prepare-se para revolucionar sua prática clínica com conhecimentos que vão além do convencional, percebendo ‘novos sons e timbres’ dessa orquestra.';
  $dataEpisodio01 = '21/10 • 20H';
  $caminhoEp01    = 'assets/farmacoflixss/videos/ep_1.mp4';
  $linkYoutubEp01 = 'https://www.youtube.com/watch?v=AOqRpRX77-g';
  $caminhoImgEp01 = 'assets/farmacoflixss/EPISODIO_01.webp';
  // EPISODIO 02
  $tituloEpisodio02 = 'Adagio: Explorando o Parassimpático';
  $textoEpisodio02  = 'Costumando a se situar-se entre 66 e 76 batidas (ou pulsações) por minuto, coincidindo com a média de batidas do seu coração quando está em repouso…Se você acha que nessa hora não tem ‘orquestra’ funcionando, enganou-se. É nesse momento de calma e repouso que o sistema parassimpático impera, diminuindo a pressão arterial, ajudando na digestão, na micção e até na reprodução humana! Quer saber como isso acontece e ainda sob influência de vários medicamentos (anti-Alzheimer, antiasmáticos, antiespasmódicos, antidepressivos, antipsicóticos e diversos colírios)? Não perca esse episódio incrível!';
  $dataEpisodio02 = '22/10 • 20H';
  $caminhoEp02    = 'assets/farmacoflixss/videos/ep_2.mp4';
  $linkYoutubEp02 = 'https://www.youtube.com/watch?v=CSFblMrpZ6Q';
  $caminhoImgEp02 = 'assets/farmacoflixss/EPISODIO_02.webp';
  // EPISODIO 03
  $tituloEpisodio03 = 'Allegro: Desvendando o Simpático';
  $textoEpisodio03  = 'Allegro (‘alegre’ em italiano) é um andamento musical leve e ligeiro que costuma ser de maior dificuldade técnica para os músicos que executam. Da mesma forma, o sistema simpático do corpo humano entra em ação em momentos de “luta e fuga”, gerando taquicardia, broncodilatação, aumento da pressão arterial, sudorese e muita agitação. Mas como cada detalhe desse acontece em questão de segundos? Por que um simples antigripal ou até um antidepressivo podem também influenciar nessas respostas? Essas questões somente podem ser compreendidas com habilidade se o profissional de saúde estiver fundamentado no conhecimento farmacológico do sistema simpático.';
  $dataEpisodio03 = '23/10 • 20H';
  $caminhoEp03    = 'assets/farmacoflixss/videos/ep3.mp4';
  $linkYoutubEp03 = 'https://www.youtube.com/watch?v=6iseJMNgggA';
  $caminhoImgEp03 = 'assets/farmacoflixss/EPISODIO_03.webp';
  // EPISODIO 04
  $tituloEpisodio04 = 'Grand Finale: Harmonia Autônoma com Casos Clínicos';
  $textoEpisodio04  = 'O ‘grand finale’ é o momento mais impactante de um espetáculo, marcado por uma apresentação final memorável. Em nosso último episódio desta série, não será diferente. Através de diversos casos clínicos, sua mente estará ‘afinada’ para compreender as mais variadas ‘melodias’ dessa orquestra silenciosa do corpo humano. Sua nova compreensão sobre diversos fármacos que atuam no sistema nervoso autônomo nunca mais será a mesma.';
  $dataEpisodio04 = '24/10 • 20H';
  $caminhoEp04    = 'assets/farmacoflixss/videos/ep_4.mp4';
  $linkYoutubEp04 = 'https://www.youtube.com/watch?v=j2lQMvd5rd4';
  $caminhoImgEp04 = 'assets/farmacoflixss/EPISODIO_04.webp';


  $legendaCards = 'Farmacologia • Medicamentos • Saúde';
  $linkWhatsapp = 'https://sendflow.pro/i/farmacoflixsinfoniasilenciosa';
  $linkCTA      = 'https://sendflow.pro/i/farmacoflixsinfoniasilenciosa';
?>

<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark-transparent" id="esconder-perfil1">
  <!-- Container wrapper -->
  <div class="container-fluid">
    <!-- Navbar brand -->
    <a class="navbar-brand" href="#">
      <img src="assets/farmacoflixcd/logoFlix.svg" height="30" alt="" loading="lazy" />
    </a>


    <div class="navbar-nav mb-lg-0 show-element-topnav">
      <!-- Navbar dropdown -->
      <div class="nav-item dropdown">
        <a class="nav-link dropdown-toggle dropbtn p-3" onclick="notificacoesResponsiva('showNotificationsCellphone')">
          <i class="fas fa-bell"></i>
          <!-- <img src="assets/farmacoflixcd/Ativo-1-1.svg" alt="" class="card-img"> -->
        </a>

        <div class="card dropdown-content grey-background" id="showNotificationsCellphone" style="right: 0;">
          <div class="card-body">

            <div class="row">
              <div class="col-4 col-md-4">
                <img src="assets/farmacoflixcd/comunidadeBanner.webp" alt="" class="card-img">
              </div>
              <div class="col-8 col-md-8 f-size-0-8 text-color-white">
                As inscrições para a Comunidade Farmaco na Prática com <strong>R$ 600</strong> de desconto abrem na quinta feira às 21h.
                <br><br>
                <a class="entrar-grupo-wpp text-weight-bolder" target="__blank" href="<?= $linkWhatsapp ?>">Entrar no grupo do Whatsapp</a>

              </div>
            </div>
            <hr class="white-hr">

            <div class="row">
              <div class="col-4 col-md-4">
                <img src="assets/farmacoflixcd/lembreteYoutube.webp" alt="" class="card-img">
              </div>
              <div class="col-8 col-md-8 f-size-0-8 text-color-white">
                Defina o lembrete das aulas e ative as notificações para não perder nenhum detalhe desta série.
                <br><br>
                <a class="ativar-lembrete text-weight-bolder" target="__blank" href="https://youtube.com/c/farmacologianapratica">Ativar o lembrete</a>

              </div>
            </div>

          </div>
        </div>

      </div>

      <div class="nav-item dropdown">
        <a class="nav-link" href="#esconder-perfil1" id="navbarDropdown" role="button" data-mdb-toggle="dropdown"
          aria-expanded="false" onclick="abrirPerfis()">
          <img src="assets/farmacoflixcd/perfil2.webp" class="img-fluid change-user-image-cell"
            height='25' width='25'>
        </a>
      </div>

    </div>


    <!-- Toggle button -->
    <button class="navbar-toggler" type="button" data-mdb-toggle="collapse" data-mdb-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" onclick="menuResponsivo()">
      <i class="fas fa-bars" id="changeIconMenuResponsive"></i>
    </button>

    <!-- Collapsible wrapper -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      <!-- Left links -->
      <ul class="navbar-nav me-auto mb-2 mb-lg-0 centralizarObjetoCelular">
        <li class="nav-item">
          <a class="nav-link" href="#">Início</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="https://instagram.com/farmaconapratica?igshid=YmMyMTA2M2Y=" target="__blank">Instagram</a>
        </li>
      </ul>
      <!-- Left links -->

      
      <div class="navbar-nav mb-2 mb-lg-0 hide-element">
        <!-- Navbar dropdown -->
        <div class="nav-item dropdown direction-rtl">
          <a class="nav-link dropdown-toggle hidden-arrow dropbtn" onclick="notificacoesResponsiva('showNotificationsDesktop')">
            <i class="fas fa-bell"></i>
          </a>


          <div class="card dropdown-content grey-background" id="showNotificationsDesktop">
            <div class="card-body direction-ltr">

              <div class="row">
                <div class="col-md-4">
                  <img src="assets/farmacoflixcd/comunidadeBanner.webp" alt="" class="card-img">
                </div>
                <div class="col-md-8 f-size-0-8 text-color-white">
                  As inscrições para a Comunidade Farmaco na Prática com <strong>R$ 600</strong> de desconto abrem na quinta feira às 21h.
                  <br><br>
                  <a class="entrar-grupo-wpp text-weight-bolder" target="__blank" href="<?= $linkWhatsapp ?>">Entrar no grupo do Whatsapp</a>

                </div>
              </div>

              <hr class="white-hr">

              <div class="row">
                <div class="col-md-4">
                  <img src="assets/farmacoflixcd/lembreteYoutube.webp" alt="" class="card-img">
                </div>
                <div class="col-md-8 f-size-0-8 text-color-white">
                  Defina o lembrete das aulas e ative as notificações para não perder nenhum detalhe desta série.
                  <br><br>
                  <a class="ativar-lembrete text-weight-bolder" target="__blank" href="https://www.youtube.com/c/farmacologianapratica/videos">Ativar o lembrete</a>

                </div>
              </div>

            </div>
          </div>

        </div>

        <div class="nav-item dropdown">
          <a class="nav-link" href="#" id="navbarDropdown" role="button" data-mdb-toggle="dropdown"
            aria-expanded="false" onclick="abrirPerfis()">
            <img src="assets/farmacoflixcd/perfil2.webp" class="img-fluid change-user-image-desk"
              height='25' width='25'>
          </a>
        </div>

      </div>

    </div>
    <!-- Collapsible wrapper -->
  </div>
  <!-- Container wrapper -->
</nav>
<!-- Navbar -->


<video autoplay muted playsinline loop id="myVideo">
    <source src="assets/farmacoflixss/videos/VIDEO_FUNDO2.mp4" type="video/mp4">
</video>

<div class="show-element mt-neg-2"></div>

<div class="content" id="esconder-perfil2">
  <div class="show-element mt-neg-35"></div>
  <div class="row">
    <div class="logo-container row align-items-center">
        <img src="assets/farmacoflixcd/fLogo.svg" width="22px" height="50px" class="card-img card-img-logo flogo">
        <p class="logo-text px-0 mb-0">SÉRIE</p>
    </div>
    <div class="col-md-5">
      <img src="assets/farmacoflixss/ativo2.webp" alt="" class="card-img mt-3">

      <div class="row">
        <div class="col-2 col-md-1-5 mt-3 float-left">
          <img src="assets/farmacoflixcd/top1.svg" width="30px" height="30px" class="card-img">
        </div>
        <div class="col-10 col-md-10 f-size-1-4 text-weight-bold mt-3">
          Internet Inteira: top 1 de hoje
        </div>
      </div>

      <div class="show-element mt-neg-10"></div>
      <p class="mt-5 font-weight-medium f-size-1">
        Uma série de 4 aulas onde vou te mostrar o funcionamento do sistema nervoso autônomo, os problemas desencadeados pelo uso incorreto de medicamentos através de casos clínicos e como definir a melhor estratégia farmacológica para diferentes situações
      </p>
    </div>
  </div>

  <!-- <div class="row">
    <div class="col-5 col-md-5">
      <a target="blank" href="https://www.youtube.com/c/farmacologianapratica">
        <button class="btn btn-netflix mt-2 col-12 col-md-3">
          <i class="fas fa-play"></i>
          Assistir
        </button>
      </a>
    </div>

    <div class="col-2 col-md-6 col-right remove-right mt-3">
    <div class="show-element mt-neg-15"></div>
      <button class="btn btn-volume position-relative" onclick="toggleMute()">
        <i aria-hidden="true" class="fas fa-volume-mute position-absolute" id="myIcon"></i>
      </button>
    </div>

    <div class="col-md-1 mt-3 hide-element">  
      <div class="row">

        <div class="col-md-12">

          <div class="card card-background-age bg-dark-transparent">
            <div class="card-body">
    
              <div class="card red-background card-age-red">
                <div class="card-body f-size-1 text-weight-bolder">
                  14
                </div>
              </div>
    
            </div>
          </div>

        </div>
      </div>
      
    </div>
  </div> -->


  </div>
</div>
</div>


<div class="container-fluid grey-background" id="esconder-perfil3">

  <div class="row mt-neg-2 hide-element">
    <div class="col-md-3 container-cards">

      <div class="card-banner-hover">
        <div>
          <img src="<?= $caminhoImgEp01 ?>" alt="" class="card-img">
        </div>

        <p class="text-color-white f-size-1 text-weight-bolder"><?= $tituloEpisodio01 ?></p>

        <div class="content-conteudo">
          <div class="content-video">
            <video autoplay muted playsinline loop height="100%" width="100%" id="videos-banners">
                <source src="<?= $caminhoEp01 ?>" type="video/mp4">
            </video>
          </div>

          <div class="controls-video">
            
              <a href="#" class="openEp01">
                <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
              </a>
              <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 1)"><i class="fas fa-thumbs-up"></i></button>
              <span class="relevance f-size-0-6">100% relevante</span>
              <span class="text-weight-bold f-size-0-6">Aula 1 de 4</span>
          </div>

          <a href="#" class="openEp01">
            <button class="btn btn-definir-lembrete col-12 f-size-1-2 lh-1-2 text-center">
                <i class="fas fa-play"></i>
                <span class="p-2">Assista Agora</span> 
            </button>
          </a>

          <div class="p-2">
              <p class="title-ep f-size-0-8 text-weight-bolder mt-2">Episódio 01 • <?= $dataEpisodio01 ?></p>

              <p class="text-weight-bolder text-color-white tituloEpisodio mt-2"><?= $tituloEpisodio01 ?></p>

              <p class="f-size-0-7" style="color: rgb(115, 115, 115);">
                  <?= $textoEpisodio01 ?>
                  <br><br>
                  <?= $legendaCards ?>
              </p>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-3 container-cards">

      <div class="card-banner-hover">
        <div>
          <img src="<?= $caminhoImgEp02 ?>" alt="" class="card-img">
        </div>

        <p class="text-color-white f-size-1 text-weight-bolder"><?= $tituloEpisodio02 ?></p>

        <div class="content-conteudo">
          <div class="content-video">
            <video autoplay muted playsinline loop height="100%" width="100%" id="videos-banners">
                <source src="<?= $caminhoEp02 ?>" type="video/mp4">
            </video>
          </div>

          <div class="controls-video">
            
              <a href="<?= $linkYoutubEp02 ?>" target="__blank">
                <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
              </a>
              <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 2)"><i class="fas fa-thumbs-up"></i></button>
              <span class="relevance f-size-0-6">100% relevante</span>
              <span class="text-weight-bold f-size-0-6">Aula 2 de 4</span>
          </div>

          <a href="<?= $linkYoutubEp02 ?>" target="__blank">
            <button class="btn btn-definir-lembrete col-12 f-size-1-2 lh-1-2 text-center">
                <i class="fas fa-play"></i>
                <span class="p-2">Assista Agora</span> 
            </button>
          </a>

          <div class="p-2">
              <p class="title-ep f-size-0-8 text-weight-bolder mt-2">Episódio 02 • <?= $dataEpisodio02 ?></p>

              <p class="text-weight-bolder text-color-white tituloEpisodio mt-2"><?= $tituloEpisodio02 ?></p>

              <p class="f-size-0-7" style="color: rgb(115, 115, 115);">
                <?= $textoEpisodio02 ?>
                <br><br>
                <?= $legendaCards ?>
              </p>
          </div>
        </div>
      </div>

    </div>

    <div class="col-md-3 container-cards">

      <div class="card-banner-hover">
        <div>
          <img src="<?= $caminhoImgEp03 ?>" alt="" class="card-img">
        </div>

        <p class="text-color-white f-size-1 text-weight-bolder"><?= $tituloEpisodio03 ?></p>

        <div class="content-conteudo">
          <div class="content-video">
            <video autoplay muted playsinline loop height="100%" width="100%" id="videos-banners">
                <source src="<?= $caminhoEp03 ?>" type="video/mp4">
            </video>
          </div>

          <div class="controls-video">
            
            <a href="<?= $linkYoutubEp03 ?>" target="__blank">
              <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
            </a>
            <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 3)"><i class="fas fa-thumbs-up"></i></button>
            <span class="relevance f-size-0-6">100% relevante</span>
            <span class="text-weight-bold f-size-0-6">Aula 3 de 4</span>
          </div>

          <a href="<?= $linkYoutubEp03 ?>" target="__blank">
            <button class="btn btn-definir-lembrete col-12 f-size-1-2 lh-1-2 text-center">
                <i class="fas fa-play"></i>
                <span class="p-2">Assista Agora</span> 
            </button>
          </a>

          <div class="p-2">
              <p class="title-ep f-size-0-8 text-weight-bolder mt-2">Episódio 03 • <?= $dataEpisodio03 ?></p>

              <p class="text-weight-bolder text-color-white tituloEpisodio mt-2"><?= $tituloEpisodio03 ?></p>

              <p class="f-size-0-7" style="color: rgb(115, 115, 115);">
                <?= $textoEpisodio03 ?>
                <br><br>
                <?= $legendaCards ?>
              </p>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-3 container-cards">

      <div class="card-banner-hover">
        <div>
          <img src="<?= $caminhoImgEp04 ?>" alt="" class="card-img">
        </div>

        <p class="text-color-white f-size-1 text-weight-bolder"><?= $tituloEpisodio04 ?></p>

        <div class="content-conteudo">
          <div class="content-video">
            <video autoplay muted playsinline loop height="100%" width="100%" id="videos-banners">
                <source src="<?= $caminhoEp04 ?>" type="video/mp4">
            </video>
          </div>

          <div class="controls-video">
            
            <a href="<?= $linkYoutubEp04 ?>" target="__blank">
              <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
            </a>
            <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 4)"><i class="fas fa-thumbs-up"></i></button>
            <span class="relevance f-size-0-6">100% relevante</span>
            <span class="text-weight-bold f-size-0-6">Aula 4 de 4</span>
          </div>

          <a href="<?= $linkYoutubEp04 ?>" target="__blank">
            <button class="btn btn-definir-lembrete col-12 f-size-1-2 lh-1-2 text-center">
                <i class="fas fa-play"></i>
                <span class="p-2">Assista Agora</span> 
            </button>
          </a>

          <div class="p-2">
              <p class="title-ep f-size-0-8 text-weight-bolder mt-2">Episódio 04 • <?= $dataEpisodio04 ?></p>

              <p class="text-weight-bolder text-color-white tituloEpisodio mt-2"><?= $tituloEpisodio04 ?></p>

              <p class="f-size-0-7" style="color: rgb(115, 115, 115);">
                <?= $textoEpisodio04 ?>
                <br><br>
                <?= $legendaCards ?>
              </p>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="row show-element">
    <div class="col-1"></div>
    <div class="col-10 video-card">
      <div class="content-conteudo">
              <div class="content-video">
                <video autoplay muted playsinline loop width="100%" id="videos-banners">
                    <source src="<?= $caminhoEp01 ?>" type="video/mp4">
                </video>
              </div>
              <div class="controls-video p-3">
      
                  <a href="#" class="openEp01">
                    <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
                  </a>
                  <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 1)"><i class="fas fa-thumbs-up"></i></button>
                  <span class="relevance f-size-0-6 p-2">100% relevante</span>
                  <span class="text-weight-bold f-size-0-6">Aula 1 de 4</span>
              </div>
              <div class="p-3">
                  <p class="title-ep f-size-0-8 text-weight-light mt-neg-4">Episódio 01 - <?= $dataEpisodio01 ?></p>
                  <p class="f-size-0-8 text-weight-bolder tituloEpisodio text-color-white mt-neg-2"><?= $tituloEpisodio01 ?></p>

                  <a href="#" class="openEp01">
                    <button class="btn btn-definir-lembrete col-9 f-size-1 lh-1-2 text-center mt-neg-2">
                        <i class="fas fa-play"></i>
                        <span class="p-2">Assista Agora</span>
                    </button>
                  </a>

                  <p class="f-size-0-8 mt-2 text-color-white">
                      <?= $textoEpisodio01 ?>
                      <br><br>
                      <?= $legendaCards ?>
                  </p>
              </div>
        </div>
    </div>
    <div class="col-1"></div>
  </div>

  <div class="row show-element mt-5">
    <div class="col-1"></div>
    <div class="col-10 video-card">
      <div class="content-conteudo">
        <div class="content-video">
          <video autoplay muted playsinline loop width="100%" id="videos-banners">
              <source src="<?= $caminhoEp02 ?>" type="video/mp4">
          </video>
        </div>

        <div class="controls-video  p-3">
            <a href="<?= $linkYoutubEp02 ?>" target="__blank">
              <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
            </a>
            <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 2)"><i class="fas fa-thumbs-up"></i></button>
            <span class="relevance f-size-0-6 p-2">100% relevante</span>
            <span class="text-weight-bold f-size-0-6">Aula 2 de 4</span>
        </div>

        <div class="p-3">

          <p class="title-ep f-size-0-8 text-weight-light mt-neg-4">Episódio 02• <?= $dataEpisodio02 ?></p>
          <p class="f-size-0-8 text-weight-bolder tituloEpisodio text-color-white mt-neg-2"><?= $tituloEpisodio02 ?></p>

          <a href="<?= $linkYoutubEp02 ?>" target="__blank">
            <button class="btn btn-definir-lembrete col-9 f-size-1 lh-1-2 text-center mt-neg-2">
                <i class="fas fa-play"></i>
                <span class="p-2">Assista Agora</span>
            </button>
          </a>

          <p class="f-size-0-8 mt-2 text-color-white">
            <?= $textoEpisodio02 ?>
            <br><br>
            <?= $legendaCards ?>
          </p>
        </div>
      </div>
    </div>
    <div class="col-1"></div>
  </div>

  <div class="row show-element mt-5">
    <div class="col-1"></div>
    <div class="col-10 video-card">
      <div class="content-conteudo">
        <div class="content-video">
          <video autoplay muted playsinline loop width="100%" id="videos-banners">
              <source src="<?= $caminhoEp03 ?>" type="video/mp4">
          </video>
        </div>

        <div class="controls-video  p-3">
          
          <a href="<?= $linkYoutubEp03 ?>" target="__blank">
            <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
          </a>
          <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 3)"><i class="fas fa-thumbs-up"></i></button>
          <span class="relevance f-size-0-6 p-2">100% relevante</span>
          <span class="text-weight-bold f-size-0-6">Aula 3 de 4</span>
        </div>

        <div class="p-3">

          <p class="title-ep f-size-0-8 text-weight-light mt-neg-4">Episódio 03 • <?= $dataEpisodio03 ?></p>
          <p class="f-size-0-8 text-weight-bolder tituloEpisodio text-color-white mt-neg-2"><?= $tituloEpisodio03 ?></p>

          <a href="<?= $linkYoutubEp03 ?>" target="__blank">
            <button class="btn btn-definir-lembrete col-9 f-size-1 lh-1-2 text-center mt-neg-2">
                <i class="fas fa-play"></i>
                <span class="p-2">Assista Agora</span>
            </button>
          </a>

          <p class="f-size-0-8 mt-2 text-color-white">
            <?= $textoEpisodio03 ?>
            <br><br>
            <?= $legendaCards ?>
          </p>
        </div>
      </div>
    </div>
    <div class="col-1"></div>
  </div>

  <div class="row show-element mt-5">
    <div class="col-1"></div>
    <div class="col-10 video-card">
      <div class="content-conteudo">
        <div class="content-video">
          <video autoplay muted playsinline loop width="100%" id="videos-banners">
              <source src="<?= $caminhoEp04 ?>" type="video/mp4">
          </video>
        </div>

        <div class="controls-video  p-3">
          
          <a href="<?= $linkYoutubEp04 ?>" target="__blank">
            <button class="btn btn-play rounded-button"><i class="fas fa-play"></i></button>
          </a>
          <button class="btn btn-pause rounded-button" onclick="changeLikedButton(this, 4)"><i class="fas fa-thumbs-up"></i></button>
          <span class="relevance f-size-0-6 p-2">100% relevante</span>
          <span class="text-weight-bold f-size-0-6">Aula 4 de 4</span>
        </div>

        <div class="p-3">

          <p class="title-ep f-size-0-8 text-weight-light mt-neg-4">Episódio 04 • <?= $dataEpisodio04 ?></p>
          <p class="f-size-0-8 text-weight-bolder tituloEpisodio text-color-white mt-neg-2"><?= $tituloEpisodio04 ?></p>

          <a href="<?= $linkYoutubEp04 ?>" target="__blank">
            <button class="btn btn-definir-lembrete col-9 f-size-1 lh-1-2 text-center mt-neg-2">
                <i class="fas fa-play"></i>
                <span class="p-2">Assista Agora</span>
            </button>
          </a>

          <p class="f-size-0-8 mt-2 text-color-white">
            <?= $textoEpisodio04 ?>
            <br><br>
            <?= $legendaCards ?>
          </p>
        </div>
      </div>
    </div>
    <div class="col-1"></div>
  </div>

  <div class="show-element mt-neg-15"></div>
    <div class="row">
    <div class="col-md-6 mt-5" style="position: relative">
      <img src="assets/farmacoflixss/bannerCelular.webp" alt="" class="card-img">
      <div class="hoverCountdown text-color-white">
        <span class="text-weight-bold f-size-1 countdown">&nbsp;&nbsp;&nbsp;Faltam</span> <br> 
        <span class="text-weight-bolder f-size-2-6">&nbsp; <span id="days"></span></span>  
        <span class="text-weight-bold f-size-1 countdown"> Dias</span> <br>
        <span class="text-weight-bolder f-size-2">&nbsp;&nbsp;<span id="hours"></span> &nbsp;&nbsp;&nbsp; <span id="minutes"></span> &nbsp;&nbsp;&nbsp;<span id="seconds"></span></span> <br>
        <span class="text-weight-bolder f-size-1 countdown">&nbsp;&nbsp;&nbsp;&nbsp; HRS  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MINS  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SEG</span>
      </div>
    </div>
    <div class="col-md-6 mt-5 centralizarObjetoCelular">

    <div class="show-element mt-neg-15"></div>
      <div class="row">
        <div class="col-md-5 mt-5">
          <img src="assets/farmacoflixcd/comunidadeFarmaco.webp" alt="" class="card-img logo-farmacologiaPratica-2">
        </div>
      </div>
      <p class="text-weight-extrabold mt-3 f-size-1-3 text-color-white">
        SAVE THE DATE: 24/10 às 21h - Ao vivo
        <!-- INSCRIÇÕES ABERTAS -->
      </p>
      <p class="text-weight-bold mt-3 f-size-1-2 text-color-white">
        <!-- Inscrições para a Comunidade Farmaco na Prática -->
      </p>

      <!-- <p class="text-weight-bold mt-3 f-size-1-4 text-color-white">
        Inscrições para Comunidade Farmaco na Prática
      </p> -->


      <div class="row">
        <div class="col-md-3">
          <div class="card card-desconto text-center">
            <div class="card-body text-color-white f-size-1-2 lh-1-2">
              R$ 600 de desconto!
            </div>
          </div>
        </div>
        <div class="col-md-9">
          <div class="mt-3 show-element"></div>
          <a href="<?= $linkCTA ?>" target="__blank">
            <button class="btn btn-wpp col-10 f-size-1-2 lh-1-2 text-center">
              <i class="fa-brands fa-whatsapp"></i>
              CLIQUE PARA ENTRAR NO GRUPO
              <!-- CLIQUE E GARANTA SUA VAGA -->
            </button>
          </a>
        </div>
      </div>

    </div>
  </div>
  
  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4 text-center mt-4 align-items-center">
      <img src="assets/farmacoflixcd/logoFarmacologiaPratica.webp" alt="" class="card-img logo-farmacologiaPratica">
    </div>
    <div class="col-md-4"></div>
  </div>

  <div class="row text-center text-color-white">
    <div class="col-md-4"></div>
    <div class="col-md-4">
      Copywriting
    </div>
    <div class="col-md-4"></div>
  </div>
  >
  <br><br>

</div>

<div class="black-background background-perfis">

  <div class="row">
    <div class="col-10 col-md-11"></div>
    <div class="col-2 col-md-1 text-center p-2">
      <a class="close-perfis" href="#" onclick="fecharPerfis()">
        <i class="fas fa-times"></i>
      </a>
    </div>
  </div>

  <div class="row text-color-white f-size-1-9 text-weight-bolder">
    <div class="col-md-12 text-center mt-10">
      Como você se define?
    </div>
  </div>

  <div class="row">
    <div class="col-md-2"></div>

    <div class="col-1 show-element"></div>
    
    <div class="col-5 col-md-2 mt-6 text-center">
      <img src="assets/farmacoflixcd/perfil1.webp" onclick="changeUserProfilePicture(1)" class="card-img hover-perfil">
      <p class="text-color-white mt-2 lh-1-2">
        NÃO SEI NADA, ME AJUDA
      </p>
    </div>


    <div class="col-5 col-md-2 mt-6 text-center">
      <img src="assets/farmacoflixcd/perfil2.webp" onclick="changeUserProfilePicture(2)" class="card-img hover-perfil">
      <p class="text-color-white mt-2 lh-1-2">
        TÔ APRENDENDO E TO ANIMADO
      </p>
    </div>


    <div class="col-1 show-element"></div>

    <div class="col-1 show-element"></div>

    <div class="col-5 col-md-2 mt-6 text-center">
      <img src="assets/farmacoflixcd/perfil3.webp" onclick="changeUserProfilePicture(3)" class="card-img hover-perfil">
      <p class="text-color-white mt-2 lh-1-2">
        PRONTO PARA DOMINAR A FARMACO
      </p>
    </div>

    <div class="col-5 col-md-2 mt-6 text-center">
      <img src="assets/farmacoflixcd/perfil4.webp" onclick="changeUserProfilePicture(4)" class="card-img hover-perfil">
      <p class="text-color-white mt-2 lh-1-2">
        JÁ SOU DA COMUNIDADE MAS TO AQUI
      </p>
    </div>

    <div class="col-1 show-element"></div>

    <div class="col-md-2"></div>
  </div>

  <br><br><br><br>
  <br><br><br><br>

</div>