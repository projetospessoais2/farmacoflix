<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <?php require('default/header.php'); ?>
    <title>FarmacoFlix</title>  
    <!-- - Farmacologia na Prática -->
    <link rel="stylesheet" href="css/farmacoflixCD/farmacoflix.css">
    <link rel="stylesheet" id="delayedcss" href="css/farmacoflixCD/delayed.css" disabled>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5LRL84J');</script>
    <!-- End Google Tag Manager -->
</head>

<body style="background-color: #1D1D1D;">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5LRL84J"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php require('contents/farmacoflixrm_content.php'); ?>

    <!-- CountDown Script -->
    <script>
        const second = 1000;
        const minute = second * 60;
        const hour = minute * 60;
        const day = hour * 24;
        const dateString = 'February 09 2023 21:00:00'
        
        let x = setInterval(() => getTimeRemaining(dateString), second);

        function getTimeRemaining(endtime){
            var total = Date.parse(endtime) - Date.parse(new Date());
            var seconds = Math.floor( (total/1000) % 60 );
            var minutes = Math.floor( (total/1000/60) % 60 );
            var hours = Math.floor( (total/(1000*60*60)) % 24 );
            var days = Math.floor( total/(1000*60*60*24) );

            if (total <= 0) {
                seconds = 0;
                minutes = 0;
                hours   = 0;
                days    = 0;
                clearInterval(x)
            }

            document.getElementById('days').innerText = ('0' + days.toString()).slice(-2);
            document.getElementById('hours').innerText = ('0' + hours.toString()).slice(-2);
            document.getElementById('minutes').innerText = ('0' + minutes.toString()).slice(-2);
            document.getElementById('seconds').innerText = ('0' + seconds.toString()).slice(-2);
        }
        
    </script>
    
    <script>

        // ReadyFunction
        document.onreadystatechange = function () {
            if (document.readyState == "interactive") {
                cssToActivate = document.getElementById('delayedcss')
                cssToActivate.removeAttribute('disabled');
            }
        }

        // PLAYING VIDEOS
        document.getElementById('myVideo').play();
        document.getElementById('myVideo').play();
        document.getElementById('myVideo').play();
        document.getElementById('myVideo').play();
        document.getElementById('myVideo').play();

        let esconderPerfil1 = document.getElementById("esconder-perfil1");
        let esconderPerfil2 = document.getElementById("esconder-perfil2");
        let esconderPerfil3 = document.getElementById("esconder-perfil3");
        let video           = document.getElementById("myVideo");
        let backgroundPerf  = document.querySelector(".background-perfis");
        let clickedBars     = false
        let clickedNotify   = false

        function toggleMute() {

            var video = document.getElementById("myVideo");
            var icon  = document.getElementById("myIcon");
            if (video.muted) {
            icon.classList.remove('fa-volume-mute')
            icon.classList.add('fa-volume-up')
            } else {
            icon.classList.add('fa-volume-mute')
            icon.classList.remove('fa-volume-up')
            }
            
            video.muted = !video.muted;
        }

        function abrirPerfis() {
            esconderPerfil1.classList.add('hide-on-perfis')
            esconderPerfil2.classList.add('hide-on-perfis')
            esconderPerfil3.classList.add('hide-on-perfis')
            video.classList.add('hide-on-perfis')

            // ADICIONA A CLASSE DE MOSTRAR
            backgroundPerf.classList.add('show-on-perfis')
        }

        function fecharPerfis() {
            esconderPerfil1.classList.remove('hide-on-perfis')
            esconderPerfil2.classList.remove('hide-on-perfis')
            esconderPerfil3.classList.remove('hide-on-perfis')
            video.classList.remove('hide-on-perfis')

            // REMOVE A CLASSE DE MOSTRAR
            backgroundPerf.classList.remove('show-on-perfis')
        }

        function menuResponsivo() {
            navbar     = document.getElementById("navbarSupportedContent")
            iconChange = document.getElementById("changeIconMenuResponsive")

            clickedBars = !clickedBars
            if (clickedBars) {
            navbar.classList.add("display-block-important")
            iconChange.classList.remove("fa-bars")
            iconChange.classList.add("fa-times")
            } else {
            navbar.classList.remove("display-block-important")
            iconChange.classList.add("fa-bars")
            iconChange.classList.remove("fa-times")
            }
        }

        function notificacoesResponsiva(objectID) {
            notificacoes = document.getElementById(objectID)

            clickedNotify = !clickedNotify
            if (clickedNotify) {
                notificacoes.classList.add("display-block-important")
            } else {
                notificacoes.classList.remove("display-block-important")
            }
        }

        function changeUserProfilePicture(imageSequence) {
            imageProfileC = document.querySelector('.change-user-image-cell')
            imageProfileD = document.querySelector('.change-user-image-desk')

            srcURL = "assets/farmacoflix/perfil" + imageSequence + ".webp"

            imageProfileC.src = srcURL
            imageProfileD.src = srcURL

            fecharPerfis()
        }

        let clickedContentConteudo = false

        function showContentConteudo(contentID) {
            contentID = 'contentConteudo' + contentID
            element = document.getElementById(contentID)

            clickedContentConteudo = !clickedContentConteudo
            if (clickedContentConteudo) {
                element.classList.add('opacity1-important')
            } else {
                element.classList.remove('opacity1-important')
            }

        }

        let clickedOnLike1 = false;
        let clickedOnLike2 = false;
        let clickedOnLike3 = false;
        let clickedOnLike4 = false;

        function changeLikedButton(button, sequence) {

            if (sequence == 1) {
            clickedOnLike1 = !clickedOnLike1

            if (clickedOnLike1) {
                button.classList.add("thumb-liked-style")
            } else {
                button.classList.remove("thumb-liked-style")
            }

            } else if (sequence == 2) {
            clickedOnLike2 = !clickedOnLike2

            if (clickedOnLike2) {
                button.classList.add("thumb-liked-style")
            } else {
                button.classList.remove("thumb-liked-style")
            }

            } else if (sequence == 3) {
            clickedOnLike3 = !clickedOnLike3

            if (clickedOnLike3) {
                button.classList.add("thumb-liked-style")
            } else {
                button.classList.remove("thumb-liked-style")
            }

            } else if (sequence == 4) {
            clickedOnLike4 = !clickedOnLike4

            if (clickedOnLike4) {
                button.classList.add("thumb-liked-style")
            } else {
                button.classList.remove("thumb-liked-style")
            }

            }
        }
    </script>
</body>

<?php //require('default/footer.php'); ?>

</html>