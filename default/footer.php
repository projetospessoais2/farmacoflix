<!-- Footer -->
<footer class="text-center text-lg-start bg-light text-muted">
  <!-- Copyright -->
  <div class="p-3 text-color-white" style="background-color: #1D1D1D;">
    <div class="f-size-1 text-center">
      <span class="text-weight-bolder">BODS MÍDIAS</span> | CNPJ: 33.894.674/0001-36 | eu@bodsmidias.com.br | Todos os direitos reservados
    </div>

  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->