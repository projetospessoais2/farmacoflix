<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="initial-scale=1" />

    <link rel="icon" type="image/x-icon" href="assets/farmacoflixss/favicon.ico">
    <title>FarmacoFlix</title>  
    <!-- - Farmacologia na Prática -->
    <link rel="stylesheet" href="css/farmacoflixSS/farmacoflix.css">
    <link rel="stylesheet" id="delayedcss" href="css/farmacoflixSS/delayed.css" disabled>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5LRL84J');</script>
    <!-- End Google Tag Manager -->
</head>

<body style="background-color: #1D1D1D;">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5LRL84J"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <script type="module" src="https://cdn.jsdelivr.net/npm/@justinribeiro/lite-youtube@1.5.0/lite-youtube.js"></script>

    <?php require('contents/farmacoflixss_content.php'); ?>

    <!-- Modal Episodios -->
    <?php require('modals/sintonia-silenciosa/ep1.php'); ?>

    <script src="js/sintonia_silenciosa.js"></script>
</body>

<?php //require('default/footer.php'); ?>

</html>